## Installation instructions

### Install dependencies

`npm install`

### Run the instrumentation

`nyc instrument test dist`

### Expected result

The instrumented code is present in the `dist` directory.

### Actual result

There is no `dist` directory.

